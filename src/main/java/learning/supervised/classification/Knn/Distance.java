package learning.supervised.classification.Knn;

import learning.supervised.bean.LearningDataPair;

/**
 * Created with IntelliJ IDEA.
 * User: prashanth.v
 * Date: 7/5/13
 * Time: 10:03 AM
 * To change this template use File | Settings | File Templates.
 */
public abstract class Distance {
    public abstract double getDistance(LearningDataPair a,LearningDataPair b);
    boolean MIN_PRECEDENCE;

    protected Distance() {
        this.MIN_PRECEDENCE =true;
    }

    protected Distance(boolean MIN_PRECEDENCE) {
        this.MIN_PRECEDENCE = MIN_PRECEDENCE;
    }

    public boolean isMIN_PRECEDENCE() {
        return MIN_PRECEDENCE;
    }

    public abstract void setPrecedence(boolean MIN_PRECEDENCE) ;
}

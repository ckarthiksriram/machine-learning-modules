package learning.supervised.classification.decisionTrees;

import learning.supervised.bean.LearningDataPair;
import learning.supervised.classification.databean.Attribute;
import learning.supervised.classification.databean.AttributeType;
import learning.supervised.classification.databean.ClassificationDataSet;

import java.util.ArrayList;
import java.util.Set;
import java.util.SortedMap;
import java.util.TreeMap;

/**
 * User: prashanth.v
 * Date: 7/7/13
 * Time: 11:48 PM
 */
public class DecisionTree {
    ClassificationDataSet dataSet;
    int numAttributes;
    AttributeType[] types;
    String[] attributeNames;
    AttributeType[] attributeTypes;
    int numClasses;
    TreeNode root;

    DecisionTree(ClassificationDataSet dataSet) {
        this.numAttributes = dataSet.getNumAttributes();
        this.numClasses = dataSet.getNumClasses();
        this.dataSet = dataSet;
        this.root = new TreeNode(numClasses);
    }

    private double computeEntropy(ClassificationDataSet data) throws Exception {

        double[] classCounts = new double[numClasses];
        for (int i = 0; i < data.getSize(); i++) {
            LearningDataPair dataPair = (LearningDataPair) data.getDataSet().get(i);
            classCounts[(int) Math.floor(dataPair.getIdeal(0))]++;
        }
        double entropy = 0;
        for (int j = 0; j < numClasses; j++) {
            if (classCounts[j] > 0) {
                entropy -= classCounts[j] * log2(classCounts[j]);
            }
        }
        entropy /= (double) data.getSize();
        return entropy + log2(data.getSize());
    }

    public double log2(double a) {
        return Math.log(a) / Math.log(2);
    }

    public boolean isUsedAttribute(TreeNode node, int attribute) {
        if (node.successor != null) {
            if (node.getDecompositionAttribute() == attribute) {
                return true;
            }
        }
        return node.getParent() != null && isUsedAttribute(node.getParent(), attribute);
    }

    public void makeTree(TreeNode node) throws Exception {

        double minEntropy;
        boolean selected = false;
        int attributeId = 0;
        int numData = node.getData().getSize();
        int numAttributesInput = numAttributes - 1;
        double initialEntropy = minEntropy = computeEntropy(node.getData());
        node.setEntropy(initialEntropy);
        if (node.getEntropy() == 0) return;
        ArrayList<TreeMap<Double, ArrayList<LearningDataPair>>> attributeWiseMap = new ArrayList<TreeMap<Double, ArrayList<LearningDataPair>>>();
        for (int i = 0; i < numAttributesInput; i++) {
            TreeMap<Double, ArrayList<LearningDataPair>> arrangedPairs = splitData(node.getData(), node.getData().getAttribute(i));
            attributeWiseMap.add(arrangedPairs);
            for (Double key : arrangedPairs.keySet()) {
                if (isUsedAttribute(node, i)) continue;
                ClassificationDataSet subset = getValueSubset(arrangedPairs,attributeTypes[attributeId] , key, node.getData());
                if (subset.getSize() == 0) continue;
                ClassificationDataSet complement = getDifferentValueSubset(node.getData(), subset);
                double e1 = computeEntropy(subset);
                double e2 = computeEntropy(complement);
                double entropy = (e1 * subset.getSize() + e2 * complement.getSize()) / numData;
                if (entropy < minEntropy) {
                    selected = true;
                    minEntropy = entropy;
                    attributeId = i;
                }
            }
        }
        if (!selected) return;
        int numValues = node.getData().getAttribute(attributeId).getNumValues();
        node.setDecompositionAttribute(attributeId);
        node.successor = new TreeNode[numValues];
        TreeMap<Double, ArrayList<LearningDataPair>> mapSelectedNode = attributeWiseMap.get(attributeId);
        int k = 0;
        for (Double key : mapSelectedNode.keySet()) {
            node.successor[k] = new TreeNode(getValueSubset(mapSelectedNode, attributeTypes[attributeId], key, node.getData()));
            node.successor[k].setParent(node);
            node.successor[k++].setDecompositionValue(key);
        }
        for (int j = 0; j < mapSelectedNode.size(); j++) {
            makeTree(node.successor[j]);
        }

        node.setData(null);

    }

    public ClassificationDataSet getDifferentValueSubset(ClassificationDataSet data, ClassificationDataSet oldset) {
        ArrayList<LearningDataPair> subset = new ArrayList<LearningDataPair>();
        int size = data.getSize();
        for (int i = 0; i < size; i++) {
            LearningDataPair point = data.getDataSet().get(i);
            int index = oldset.getDataSet().indexOf(point);
            if (index < 0) subset.add(point);
        }
        return new ClassificationDataSet(subset, numClasses);


    }

    public void printTree(TreeNode node, String tab) {

        int outputattr = numAttributes - 1;
        if (node.successor == null) {
            Double[] values = getAtrributeVal(node.getData(), outputattr);

            if (values.length == 1) {
                System.out.println(tab + "\t" + attributeNames[outputattr] + " = \"" + values[0] + "\";");
                return;
            }
            System.out.print(tab + "\t" + attributeNames[outputattr] + " = {");
            for (int i = 0; i < values.length; i++) {
                System.out.print("\"" + (values[i]) + "\" ");
                if (i != values.length - 1) System.out.print(" , ");
            }
            System.out.println(" };");
            return;
        }

        int numValues = node.successor.length;

        for (int i = 0; i < numValues; i++) {

            System.out.println(tab + "if( " + attributeNames[node.getDecompositionAttribute()] + (attributeTypes[node.getDecompositionAttribute()]==AttributeType.DISCRETE? " == \"":"<=") + node.successor[i].getDecompositionValue() + "\") {");
            printTree(node.successor[i], tab + "\t");
            if (i != numValues - 1) System.out.print(tab + "} else ");
            else System.out.println(tab + "}");

        }
    }

    public ClassificationDataSet getValueSubset(TreeMap<Double, ArrayList<LearningDataPair>> arrangedPairs, AttributeType type, Double value, ClassificationDataSet data) {
        if (type == AttributeType.CONTINUOUS) {
            SortedMap<Double, ArrayList<LearningDataPair>> lessThanMap = arrangedPairs.headMap(value, true);
            ArrayList<LearningDataPair> finalDataPair = new ArrayList<LearningDataPair>();
            for (Double key : lessThanMap.keySet()) {
                finalDataPair.addAll(lessThanMap.get(key));
            }
            return new ClassificationDataSet(finalDataPair, numClasses);//;getValueSubset(node.getData(), i, j);
        }
        return new ClassificationDataSet(arrangedPairs.get(value), numClasses);//;getValueSubset(node.getData(), i, j);
    }

    private TreeMap<Double, ArrayList<LearningDataPair>> splitData(ClassificationDataSet data, Attribute att) {
        TreeMap<Double, ArrayList<LearningDataPair>> dataPairs = new TreeMap<Double, ArrayList<LearningDataPair>>();
        //    ClassificationDataSet[] splitData = new ClassificationDataSet[att.getNumValues()];
        for (LearningDataPair pair : data.getDataSet()) {
            ArrayList<LearningDataPair> pairVal;
            if ((pairVal = dataPairs.get(pair.value(att.getIndex()))) == null) {
                pairVal = new ArrayList<LearningDataPair>();
            }
            pairVal.add(pair);
            dataPairs.put(pair.value(att.getIndex()), pairVal);
        }
        return dataPairs;
    }

    public int init(String filename, ClassificationDataSet dataSet) throws Exception {
        numAttributes = dataSet.getNumAttributes() + 1;//tokenizer.countTokens();
        attributeNames = new String[numAttributes];
        attributeNames = new String[]{"Outlook", "Temp", "Humidity", "Wind", "Play"};
        attributeTypes=new AttributeType[]{AttributeType.DISCRETE,AttributeType.CONTINUOUS,AttributeType.DISCRETE,AttributeType.DISCRETE,AttributeType.DISCRETE} ;
        root.setData(dataSet);
        return 1;

    }

    public void createDecisionTree() throws Exception {
        makeTree(root);
        printTree(root, "");
    }

    public Double[] getAtrributeVal(ClassificationDataSet data, int attribute) {
        ArrayList<Double> output = new ArrayList<Double>();
        if (attribute == numAttributes - 1) {
            for (int i = 0; i < data.getSize(); i++) {
                Double value = data.getDataSet().get(i).getIdeal(0);
                if (output.indexOf(value) < 0)
                    output.add(value);
            }
            return output.toArray(new Double[output.size()]);
        }
        System.out.println(data.getSize() + "attribute" + " " + attribute + " " + data.getAttribute(attribute) + " " + numAttributes);
        Set<Double> s = data.getAttribute(attribute).getUniqueAttributes().keySet();
        return s.toArray(new Double[s.size()]);

    }


    //sunny-0,overcast-1,rain-2
    //hot-0,mild-1,cool-2
    //high-0,normal-1,
    //weak-0,strong-1
    //ply-1,no-0

    public static void main(String[] args) throws Exception {
        //double[][] inputs = new double[][]{{0, 0, 0, 0}, {0, 0, 0, 1}, {1, 0, 0, 0}, {2, 1, 0, 0}, {2, 2, 1, 0}, {2, 2, 1, 1}, {1, 2, 1, 1}, {0, 1, 0, 0}, {0, 2, 1, 0}, {2, 1, 1, 0}, {0, 1, 1, 1}, {1, 1, 0, 1}, {1, 0, 1, 0}, {2, 1, 0, 1}};
        double[][] inputs = new double[][]{{0, 40, 0, 0}, {0, 50, 0, 1}, {1, 60, 0, 0}, {2, 61, 0, 0}, {2, 62, 1, 0}, {2, 63, 1, 1}, {1, 62, 1, 1}, {0, 65, 0, 0}, {0, 62, 1, 0}, {2, 61, 1, 0}, {0, 61, 1, 1}, {1, 61, 0, 1}, {1, 60, 1, 0}, {2, 72, 0, 1}};
        double[][] outputs = new double[][]{{0}, {0}, {1}, {1}, {1}, {0}, {1}, {0}, {1}, {1}, {1}, {1}, {1}, {0}};
        double[] input = new double[]{10.55, 11.75};
        String attributeNames[] = new String[]{};
        ClassificationDataSet dataSet = new ClassificationDataSet(inputs, outputs, 2);
        DecisionTree decisionTree = new DecisionTree(dataSet);
        decisionTree.init("", dataSet);
        decisionTree.createDecisionTree();
        double[] output = new double[1];
    }


}

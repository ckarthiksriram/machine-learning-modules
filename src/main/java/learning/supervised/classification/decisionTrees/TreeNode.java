package learning.supervised.classification.decisionTrees;

import learning.supervised.bean.LearningDataPair;
import learning.supervised.classification.databean.ClassificationDataSet;

import java.util.ArrayList;

/**
 * Created with IntelliJ IDEA.
 * User: prashanth.v
 * Date: 7/7/13
 * Time: 11:45 PM
 * To change this template use File | Settings | File Templates.
 */
public class TreeNode {
    private double entropy;            // The entropy of data points if this node is a leaf node
    private ClassificationDataSet data;            // The set of data points if this is a leaf node
    private int decompositionAttribute;    // If this is not a leaf node, the attribute that is used to divide the set of data points
    private Double decompositionValue;        // If this is not a leaf node, the attribute-value that is used to divide the set of data points
    public TreeNode[] successor;        // If this is not a leaf node, references to the successor nodes
    private TreeNode parent;            // The parent to this node.  The root has parent == null

    public  TreeNode(int numClasses){
       data=new ClassificationDataSet(new ArrayList<LearningDataPair>(),numClasses);
    }
    public TreeNode(ClassificationDataSet dataSet) {
        data = dataSet;
    }

    public double getEntropy() {
        return entropy;
    }

    public void setEntropy(double entropy) {
        this.entropy = entropy;
    }

    public ClassificationDataSet getData() {
        return data;
    }

    public void setData(ClassificationDataSet data) {
        this.data = data;
    }

    public int getDecompositionAttribute() {
        return decompositionAttribute;
    }

    public void setDecompositionAttribute(int decompositionAttribute) {
        this.decompositionAttribute = decompositionAttribute;
    }

    public Double getDecompositionValue() {
        return decompositionValue;
    }

    public void setDecompositionValue(Double decompositionValue) {
        this.decompositionValue = decompositionValue;
    }

    public TreeNode[] getSuccessor() {
        return successor;
    }

    public void setSuccessor(TreeNode[] successor) {
        this.successor = successor;
    }

    public TreeNode getParent() {
        return parent;
    }

    public void setParent(TreeNode parent) {
        this.parent = parent;
    }
}

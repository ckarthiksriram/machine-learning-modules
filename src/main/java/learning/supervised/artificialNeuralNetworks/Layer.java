package learning.supervised.artificialNeuralNetworks;

import java.util.ArrayList;
import java.util.Random;

/**
 * Created with IntelliJ IDEA.
 * User: prashanth.v
 * Date: 7/3/13
 * Time: 2:48 PM
 * To change this template use File | Settings | File Templates.
 */
public class Layer {
    ArrayList<Neuron> neurons;
    LayerType type;
    int neuronCount;
    Random random = new Random();
    ActivationFunction function;
    Neuron bias = new Neuron();
    boolean biasAccepted;
    Layer previous;
    Layer next;


    public Layer(int count, LayerType type, ActivationFunction function, Layer previousLayer, Layer next, boolean biasAccepted) {
        this.neurons = new ArrayList<Neuron>(count);
        this.type = type;
        this.neuronCount = count;
        this.function = function;
        this.biasAccepted = biasAccepted;
        this.previous = previousLayer;
        this.next = next;
        init(previousLayer);
    }

    private void init(Layer previousLayer) {
        for (int i = 0; i < neuronCount; i++) {
            Neuron neuron = new Neuron();
            if (previousLayer != null) {
                neuron.addIncomingSynapses(previousLayer.getNeurons(), random);
                neuron.addBiasConnection(new Synapse(bias, neuron));
            }
            neurons.add(neuron);
        }
    }

    public ArrayList<Neuron> getNeurons() {
        return neurons;
    }

    public void setNeurons(ArrayList<Neuron> neurons) {
        this.neurons = neurons;
    }

    public LayerType getType() {
        return type;
    }

    public void setType(LayerType type) {
        this.type = type;
    }

    public void activate() {
        for (Neuron neuron : neurons) {
            neuron.setOutput(neuron.calculateOutput(function));
        }
    }

    public void calculateGradients(Double expectedOutput[]) {
        int i = 0;
        for (Neuron n : neurons) {
            ArrayList<Synapse> synapses = n.getIncomingSynapses();
            for (Synapse syn : synapses) {
                double ak = n.getOutput();
                double partialDerivative;
                if (type == LayerType.OUTPUT_LAYER) {
                    Double desiredOutput = expectedOutput[i];
                    partialDerivative = -function.partialDerivative(0, ak) * (desiredOutput - ak);
                } else {
                    partialDerivative = function.partialDerivative(0, ak) * sumWeightedGradient(n);
                }
                //    double deltaWeight =  -laerningRate*partialDerivative*ai;
                //  double newWeight = con.getWeight() + deltaWeight;
                syn.setGradient(partialDerivative);

                   // System.out.println(type + " " + syn.src.id + " " + syn.destination.id + "=>" + " " + partialDerivative + " " + " " + ak);
                //con.setWeight(newWeight + momentum * con.getPrevDelta());
            }
            i++;
        }
    }

    public double[] getOutputs() {
        double[] output = new double[this.getNeuronCount()];
        for (int i = 0; i < output.length; i++) {
            output[i] = this.getNeurons().get(i).getOutput();
        }
        return output;
    }


    private double sumWeightedGradient(Neuron n) {
        int j = 0;
        double sumKoutputs = 0;
        for (Neuron nextLayerNeuron : next.getNeurons()) {
            Synapse synjk = nextLayerNeuron.getConnection(n.id);
            double wjk = synjk.getWeight();
            j++;
            sumKoutputs += (synjk.getGradient() * wjk);
        }
        return sumKoutputs;
    }

    public int getNeuronCount() {
        return neuronCount;
    }

    public void setNeuronCount(int neuronCount) {
        this.neuronCount = neuronCount;
    }

    public Random getRandom() {
        return random;
    }

    public void setRandom(Random random) {
        this.random = random;
    }

    public ActivationFunction getFunction() {
        return function;
    }

    public void setFunction(ActivationFunction function) {
        this.function = function;
    }

    public Neuron getBias() {
        return bias;
    }

    public void setBias(Neuron bias) {
        this.bias = bias;
    }

    public boolean isBiasAccepted() {
        return biasAccepted;
    }

    public void setBiasAccepted(boolean biasAccepted) {
        this.biasAccepted = biasAccepted;
    }

    public Layer getPrevious() {
        return previous;
    }

    public void setPrevious(Layer previous) {
        this.previous = previous;
    }

    public Layer getNext() {
        return next;
    }

    public void setNext(Layer next) {
        this.next = next;
    }
}

package learning.unsupervised.clustering.bean;

import java.util.Arrays;

/**
 * User: prashanth.v
 * Date: 6/19/13
 * Time: 5:45 PM
 */
public class ClusterDataPoint extends DataPoint {
    int associatedCluster;
    Double distance;
    boolean visited;
    boolean noise;


    public ClusterDataPoint(Double[] dataValues) {
        super(dataValues);
        associatedCluster = -1;
        visited = false;
        noise = false;
    }

    public ClusterDataPoint(Double[] dataPt, int associatedCluster, Double distance) {
        super(dataPt);
        this.associatedCluster = associatedCluster;
        this.distance = distance;
        this.visited = false;
        this.noise = false;
        this.associatedCluster=-1;
    }

    public int getAssociatedCluster() {
        return associatedCluster;
    }

    public void setAssociatedCluster(int associatedCluster) {
        this.associatedCluster = associatedCluster;
    }

    public Double getDistance() {
        return distance;
    }

    public void setDistance(Double distance) {
        this.distance = distance;
    }

    public boolean isNoise() {
        return noise;
    }

    public void setNoise(boolean noise) {
        this.noise = noise;
    }

    public boolean isVisited() {
        return visited;
    }

    public void setVisited(boolean visited) {
        this.visited = visited;
    }

    @Override
    public int hashCode() {
        int result = 1;
        result = Arrays.hashCode(dataValues);
        return result;
    }

    @Override
    public String toString() {
        return "ClusterDataPoint{dataPt=" + Arrays.toString(dataValues) + ", " +
                "associatedCluster=" + associatedCluster +
                ", distance=" + distance +
                '}';
    }
}

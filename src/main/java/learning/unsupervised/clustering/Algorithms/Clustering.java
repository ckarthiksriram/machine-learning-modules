package learning.unsupervised.clustering.Algorithms;

import learning.unsupervised.clustering.bean.*;

import java.util.*;

/**
 * User: prashanth.v
 * Date: 6/19/13
 * Time: 4:35 PM
 */
abstract public class Clustering {
    ClusterData clusterData;

    public Clustering(Double[][] dataSet, int k) {
        clusterData = new ClusterData(dataSet, k);
    }

    public Clustering(ClusterData clusterData) {
        this.clusterData = clusterData;
    }

    public void clusterData() {
        randomCentroidAssignments();
        for (int i = 0; i < 1000; i++) {
            calculateNewCentroids();
            reassignPtsToNewCluster();
        }
    }

    private void reassignPtsToNewCluster() {
        assignToClusters();
    }

    private void randomCentroidAssignments() {
        ArrayList<ClusterDataPoint> dataSet = clusterData.getDataSet();
        int n = clusterData.getClusterCount();
        Random random = new Random();
        ClusterDataPoint clusterDataPoint = dataSet.remove(random.nextInt(dataSet.size()));
        ClusterCenters clusterCenter = new ClusterCenters(clusterDataPoint, 0);
        clusterData.addClusterCenter(clusterCenter);
        for (int j = 1; j < n; j++) {
            Double sum = 0.0;
            ArrayList<Double> dxs = new ArrayList<Double>();
            for (ClusterDataPoint dataPoint : dataSet) {
                ArrayList<Pair<Integer, Double>> distances = calculateCentroidDistances(dataPoint);
                Pair<Integer, Double> clusterDistance = Collections.min(distances);
                modifyCluster(dataPoint, clusterDistance);
                double x = clusterDistance.getSecond() * clusterDistance.getSecond();
                sum += (int) (clusterDistance.getSecond() * clusterDistance.getSecond());
                dxs.add(sum);
            }
            ClusterDataPoint features;
            double r = random.nextDouble() * dxs.get(dxs.size() - 1);
            for (int i = 0; i < dxs.size(); i++) {
                System.out.println(dxs.get(i) + " " + r + " " + Arrays.toString(dataSet.get(i).getDataValues()));
                if (dxs.get(i) >= r) {
                    features = dataSet.remove(i);
                    clusterData.addClusterCenter(new ClusterCenters(features, j));
                    break;
                }

            }
        }
    }

    //abstract public double calculateDistance(Double[] pt1Values, Double[] pt2Values);
    abstract public ArrayList<Pair<Integer, Double>> calculateCentroidDistances(final DataPoint dataPoint);
    abstract public void calculateCentroids(ClusterCenters clusterCenters);
    abstract public void assignToClusters();
    abstract public void modifyCluster(ClusterDataPoint dataPoint, Pair<Integer, Double> bestCluster);


    private void calculateNewCentroids() {
        HashMap<Integer,ClusterCenters> centers = clusterData.getClusterCenters();
        for (Map.Entry<Integer,ClusterCenters> center : centers.entrySet()) {
            calculateCentroids(center.getValue());
        }
    }

    public static void main(String[] args) {
        ArrayList<ClusterDataPoint> dataSet = new ArrayList<ClusterDataPoint>();
        Double[][] datas = new Double[][]{{2.0, 3.5}, {3.0, 5.0}, {6.0, 7.0}, {8.0, 9.0}, {1.0, 5.0}, {2.0, 5.0}, {3.0, 6.0}, {9.5, 2.0}, {0.0, 1.0}, {1.0, 2.0}, {3.0, 1.0}};
        Clustering clustering = new KMeans(datas, 3);
        clustering.clusterData();
        System.out.println((clustering.clusterData.getClusterCenters()).toString());
        for (Map.Entry<Integer,ClusterCenters> center : (clustering.clusterData.getClusterCenters().entrySet())) {
            System.out.println(center.getValue().getMemberPoints().toString());
        }
        //Clustering clustering=new KMeans();
    }


}
